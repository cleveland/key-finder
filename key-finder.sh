#!/bin/bash

# Test key name is TEST_KEY

per_page=2
page=1
headers=headers.$$

echo -n "Enter your variable name (key) and press [ENTER]: "
read key_name
echo "Your variable name is: $key_name"

echo -n "Enter your admin private token and press [ENTER]: "
read token
echo "Token set."

echo -n "Enter your hostname including http(s). For example, https://gitlab.com and press [ENTER]: "
read hostname
echo "Hostname set."

echo " "
echo "===== Running ====="
echo " "

get_id_and_pages () {
	curl -s --header "PRIVATE-TOKEN: $token" -D $headers "$hostname/api/v4/projects?per_page=$per_page&page=$page" | jq -r '.[] | .id' >> list_of_ids.txt
	xtotalpages=$(grep "X-Total-Pages" $headers | cut -d : -f2 | sed 's/[^0-9]*//g')
	xtotalpages=${xtotalpages:-1}
	if [[ $xtotalpages -gt 1 ]]
	then
		page=$((page+1))
		until [[ $page > $xtotalpages ]]
		do
			curl -s --header "PRIVATE-TOKEN: $token" "$hostname/api/v4/projects?per_page=$per_page&page=$page" | jq -r '.[] | .id' >> list_of_ids.txt
			page=$((page+1))
		done
	fi
}

find_keys () {
	while IFS= read -r id
	do
		PATH_WITH_NAMESPACE=$(curl -s --header "PRIVATE-TOKEN: $token" $hostname/api/v4/projects/$id | jq -r '.path_with_namespace')
		OUTPUT=$(curl -s --header "PRIVATE-TOKEN: $token" $hostname/api/v4/projects/$id/variables/$key_name | jq .)
		if [[ $OUTPUT =~ "404" ]]
		then
			echo $PATH_WITH_NAMESPACE  
			echo "Key Not found"
		elif [[ $OUTPUT =~ "403" ]] 
		then
			echo $PATH_WITH_NAMESPACE
			echo "Project Not found (Likely archived)"
		else
			echo $PATH_WITH_NAMESPACE | tee -a ./found_keys.txt
			echo $OUTPUT | tee -a ./found_keys.txt
		fi
		echo " "
		sleep 1
	done < "list_of_ids.txt"
}


get_id_and_pages
find_keys
rm list_of_ids.txt
rm $headers

echo " "
echo "===== A list of projects where this key was found was placed in found_keys.txt ====="
echo " "